﻿using Microsoft.AspNetCore.Http;
using System.IdentityModel.Tokens.Jwt;

namespace PortfolioManagement
{
    public class Extentions
    {
    }

    public static class IHttpContextAccessorExtension
    {
        public static int CurrentUser(this IHttpContextAccessor httpContextAccessor)
        {
            var stringId = httpContextAccessor?.HttpContext?.User?.FindFirst(JwtRegisteredClaimNames.Iat)?.Value;
            int.TryParse(stringId ?? "0", out int userId);

            return userId;
        }
    }
}
