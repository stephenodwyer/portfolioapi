﻿using System;

namespace PortfolioManagement.Dao
{
    public class Transaction
    {
        public string Company { get; set; }
        public int Quantity { get; set; }
        public DateTime Date { get; set; }
        public string Type { get; set; }
        public int StockId { get; set; }
        public int UserId { get; set; }
    }
}
