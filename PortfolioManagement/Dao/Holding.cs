﻿using System;

namespace PortfolioManagement.Dao
{
    public class Holding
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Quantity { get; set; }
        public string Comapany { get; set; }
    }
}
