﻿using System;

namespace PortfolioManagement.Models
{
    public partial class Ledger
    {
        public int StockId { get; set; }
        public DateTime Date { get; set; }
        public int Quantity { get; set; }
        public string Type { get; set; }
        public int UserId { get; set; }
        public int Id { get; set; }

        public Ledger IdNavigation { get; set; }
        public User User { get; set; }
        public Ledger InverseIdNavigation { get; set; }
    }
}
