﻿using System.Collections.Generic;

namespace PortfolioManagement.Models
{
    public partial class Compnay
    {
        public Compnay()
        {
            Price = new HashSet<Price>();
        }

        public string Symbol { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }

        public ICollection<Price> Price { get; set; }
    }
}
