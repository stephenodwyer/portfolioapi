﻿using System.Collections.Generic;

namespace PortfolioManagement.Models
{
    public partial class User
    {
        public User()
        {
            Ledger = new HashSet<Ledger>();
        }

        public string Name { get; set; }
        public byte[] Salt { get; set; }
        public byte[] Hash { get; set; }
        public int Id { get; set; }

        public ICollection<Ledger> Ledger { get; set; }
    }
}
