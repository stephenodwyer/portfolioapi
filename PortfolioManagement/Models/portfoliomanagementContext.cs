﻿using Microsoft.EntityFrameworkCore;

namespace PortfolioManagement.Models
{
    public partial class PortfolioManagementContext : DbContext
    {
        public PortfolioManagementContext()
        {
        }

        public PortfolioManagementContext(DbContextOptions<PortfolioManagementContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Compnay> Compnay { get; set; }
        public virtual DbSet<Ledger> Ledger { get; set; }
        public virtual DbSet<Price> Price { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Compnay>(entity =>
            {
                entity.ToTable("compnay");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(25);

                entity.Property(e => e.Symbol)
                    .IsRequired()
                    .HasColumnName("symbol")
                    .HasMaxLength(10);
            });

            modelBuilder.Entity<Ledger>(entity =>
            {
                entity.ToTable("ledger");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Quantity).HasColumnName("quantity");

                entity.Property(e => e.StockId).HasColumnName("stock_id");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.InverseIdNavigation)
                    .HasForeignKey<Ledger>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ledger_ledger");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Ledger)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ledger_user");
            });

            modelBuilder.Entity<Price>(entity =>
            {
                entity.HasKey(e => new { e.Date, e.StockId });

                entity.ToTable("price");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("date");

                entity.Property(e => e.StockId).HasColumnName("stock_id");

                entity.Property(e => e.Price1).HasColumnName("price");

                entity.HasOne(d => d.Stock)
                    .WithMany(p => p.Price)
                    .HasForeignKey(d => d.StockId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_price_compnay");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("user");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Hash)
                    .IsRequired()
                    .HasColumnName("hash")
                    .HasMaxLength(64);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50);

                entity.Property(e => e.Salt)
                    .IsRequired()
                    .HasColumnName("salt")
                    .HasMaxLength(8);
            });
        }
    }
}
