﻿using PortfolioDao;

using System;
using System.Security.Cryptography;
using System.Linq;
using PortfolioManagement.Models;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;
using System.Text;

namespace PortfolioServices.Services
{

    public interface IAuthService
    {
        JwtSecurityToken AuthenticateUser(Credentials user);
    }

    public class AuthService : IAuthService
    {

        public int Iterations { get; set; } = 10000;
        public HashAlgorithmName HashAlgorithm { get; set; } = HashAlgorithmName.SHA512;
        public int NumberOfKeyBytes = 64;
        PortfolioManagementContext db;
        private readonly IConfiguration configuration;

        public AuthService(PortfolioManagementContext db, IConfiguration configuration)
        {
            this.db = db;
            this.configuration = configuration;
        }

        public JwtSecurityToken AuthenticateUser(Credentials credentials)
        {
            bool validated = false;
            JwtSecurityToken token = default(JwtSecurityToken);
            User user = GetUser(credentials);

            if (user != null)
            {
                validated = ValidatePassword(credentials, user);

                if (validated)
                {
                    token = GetClaimToken(credentials, user);
                }
            }

            return token;
        }

        protected User GetUser(Credentials credentials)
        {
            User user = default(User);
            try
            {
                user = db.User
                 .Where(a => a.Name == credentials.Name)
                 .FirstOrDefault();
            }
            catch (Exception)
            {
                // TODO LOG ERROR;
            }
            return user;
        }

        public bool ValidatePassword(Credentials credentials, User user)
        {
            bool success;
            byte[] hash = GetHashPassword(user.Salt, credentials.Password);
            success = hash.SequenceEqual(user.Hash);
            return success;
        }

        public JwtSecurityToken GetClaimToken(Credentials credentials, User user)
        {
            var claims = new[]
                    {
                        new Claim(JwtRegisteredClaimNames.Sub, credentials.Name),
                        new Claim(JwtRegisteredClaimNames.Iat, user.Id.ToString())
                    };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Tokens:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(configuration["Tokens:Issuer"],
            configuration["Tokens:Issuer"],
            claims,
            expires: DateTime.Now.AddMinutes(30),
            signingCredentials: creds);

            return token;
        }

        public byte[] GetNewSalt()
        {
            byte[] salt = new byte[8];
            using (RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider())
            {
                rngCsp.GetBytes(salt);
            }

            return salt;
        }

        public byte[] GetHashPassword(byte[] salt, string password)
        {
            Rfc2898DeriveBytes k1 = new Rfc2898DeriveBytes(
                password,
                salt,
                Iterations,
                HashAlgorithm
                );

            return k1.GetBytes(NumberOfKeyBytes);
        }
    }
}
