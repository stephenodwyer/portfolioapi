﻿using PortfolioManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PortfolioManagement.Dao;

namespace PortfolioManagement.Services
{

    public interface IStockService
    {
        Task<List<Holding>> GetStockHoldings(int userId);
    }

    public class StockService : IStockService
    {
        private readonly PortfolioManagementContext db;

        public StockService(PortfolioManagementContext db)
        {
            this.db = db;
        }

        public Task<List<Holding>> GetStockHoldings(int userId)
        {
            return Task.Run(() =>
            {
                var groupTransactions =
                (from entry in db.Ledger
                 join user in db.User on entry.UserId equals user.Id
                 where user.Id == userId
                 orderby entry.Date
                 group entry by new
                 {
                     entry.StockId,
                     entry.Type,
                     entry.Date

                 }).ToList().Select(trans => new Transaction()
                 {
                     Company = db.Compnay
                    .Where(x => x.Id == trans.Key.StockId)
                    .Select(x => x.Name).First(),
                     StockId = trans.Key.StockId,
                     Type = trans.Key.Type,
                     Quantity = trans.Select(x => x.Quantity).Sum(),
                     Date = trans.Key.Date,
                     UserId = userId
                 }).GroupBy(x => x.Company);

                // https://github.com/aspnet/EntityFrameworkCore/issues/12560 
                // Weird Core version bug. Needed to ToList after group by

                List<Holding> holdings = new List<Holding>();

                foreach (var company in groupTransactions)
                {
                    List<Holding> companyHoldings = new List<Holding>();

                    foreach (var tran in company)
                    {
                        int lastHoldingQuantity = 0;
                        if (companyHoldings.Count > 0)
                        {
                            companyHoldings.Last().EndDate = tran.Date;
                            lastHoldingQuantity = companyHoldings.Last().Quantity;
                        }

                        Holding holding = new Holding()
                        {
                            StartDate = tran.Date,
                            Comapany = company.Key,
                            EndDate = DateTime.Now
                        };

                        if (tran.Type == "BUY")
                        {
                            holding.Quantity = lastHoldingQuantity + tran.Quantity;
                        }
                        else
                        {
                            holding.Quantity = lastHoldingQuantity - tran.Quantity;
                        }

                        companyHoldings.Add(holding);
                    }

                    holdings.AddRange(companyHoldings);
                }

                return holdings;
            });
        }
      
    }
}
