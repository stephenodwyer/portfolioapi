﻿using System.ComponentModel.DataAnnotations;

namespace PortfolioDao
{
    public class Credentials
    {
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 8)]
        public string Password { get; set; }
        
        public Credentials(string name, string password)
        {
            Name = name;
            Password = password;
        }

    }
}
