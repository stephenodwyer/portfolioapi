﻿using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PortfolioDao;
using PortfolioServices.Services;

namespace PortfolioManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IAuthService authService;
        private readonly IConfiguration configuration;

        public LoginController(IAuthService authService, IConfiguration configuration)
        {
            this.authService = authService;
            this.configuration = configuration;
        }

        // POST: api/Login
        [HttpPost]
        [AllowAnonymous]
        public IActionResult Post([FromBody] Credentials user)
        {
            JwtSecurityToken token = authService.AuthenticateUser(user);
            if (token != null)
            {
                return Ok(new { token = new JwtSecurityTokenHandler().WriteToken(token) });
            }
             return BadRequest("Could not create token");
        }
    }
}
