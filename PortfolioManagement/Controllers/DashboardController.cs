﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PortfolioManagement.Dao;
using PortfolioManagement.Services;

namespace PortfolioManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DashboardController : ControllerBase
    {
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly IStockService stockService;

        public DashboardController(IHttpContextAccessor httpContextAccessor, IStockService stockService)
        {
            this.httpContextAccessor = httpContextAccessor;
            this.stockService = stockService;
        }
        // GET: api/Dashboard
        [HttpGet]
        public async Task<List<Holding>> GetAsync()
        {
            try
            {
                return await stockService.GetStockHoldings(httpContextAccessor.CurrentUser());
            }
            catch (Exception)
            {
                // TODO Add Error handling
            }

          return await Task.FromResult(new List<Holding>());
        }
    }
}
